#!/usr/bin/env bash
set -e
echo -n $DOCKER_REGISTRY_PASSWORD | docker login --username $DOCKER_REGISTRY_USERNAME --password-stdin $DOCKER_REGISTRY

IMAGE="$DOCKER_IMAGE:$DOCKER_TAG"
echo "Building $IMAGE"
docker build --build-arg model_name=model -t $IMAGE .
echo "Pushing $IMAGE"
docker push $IMAGE

# update the image for the dotscience build so we can deploy it
curl \
  -X PUT \
  -H 'Content-type: application/json' \
  -d '{"image_name":"'$IMAGE'"}' \
  -u $DOTSCIENCE_USERNAME:$DOTSCIENCE_API_KEY \
  $DOTSCIENCE_API_HOSTNAME/v2/models/$DOTSCIENCE_MODEL_ID/builds/$DOTSCIENCE_MODEL_BUILD_ID

echo
echo "-------------------------------------------------------------"
echo
echo "Your model has been built - you can see the dotscience build"
echo "status in the https://cloud.dotscience.com/models/builds page"
echo
echo "-------------------------------------------------------------"
echo